# rtmpdump

RTMPDUMP_VERSION := 2.2e
RTMPDUMP_URL := http://rtmpdump.mplayerhq.hu/download/rtmpdump-$(RTMPDUMP_VERSION).tar.gz

PKGS += rtmpdump
ifeq ($(call need_pkg,"rtmpdump"),)
PKGS_FOUND += rtmpdump
endif

$(TARBALLS)/rtmpdump-$(RTMPDUMP_VERSION).tar.gz:
	$(call download,$(RTMPDUMP_URL))

.sum-rtmpdump: rtmpdump-$(RTMPDUMP_VERSION).tar.gz

rtmpdump: rtmpdump-$(RTMPDUMP_VERSION).tar.gz .sum-rtmpdump
	$(UNPACK)
	echo $(basename $(notdir $<))
	$(APPLY) $(SRC)/rtmpdump/makefile.patch
	$(APPLY) $(SRC)/rtmpdump/diff-array-and-object.patch
	$(MOVE)

.rtmpdump: rtmpdump
	cd $< && $(HOSTVARS) prefix="$(PREFIX)" $(MAKE) -C librtmp install
	touch $@

