/*****************************************************************************
 * flv.c: play SINA(and more) video without switching between files
 *****************************************************************************
 *
 * Authors: Ming Hu <tewilove@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <vlc_common.h>
#include <vlc_plugin.h>
#include <vlc_threads.h>
#include <vlc_arrays.h>
#include <vlc_stream.h>
#include <vlc_url.h>
#include <vlc_memory.h>
#include <vlc_md5.h>

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

#include <librtmp/amf.h>
#include <librtmp/log.h>
#include <librtmp/rtmp.h>

#define FLV_FILTER_MODE_TEXT N_("Mode")
#define FLV_FILTER_MODE_LONGTEXT N_("TODO")
#define FLV_FILTER_BUFFER_SIZE_TEXT N_("Buffer Size")
#define FLV_FILTER_BUFFER_SIZE_LONGTEXT N_("TODO")
#define FLV_FILTER_OUTPUT_FILE_TEXT N_("Output")
#define FLV_FILTER_OUTPUT_FILE_LONGTEXT N_("TODO")
#define FLV_FILTER_SEEK_METHOD_TEXT N_("Seek Method")
#define FLV_FILTER_SEEK_METHOD_LONGTEXT N_("TODO")

static int  Open (vlc_object_t *);
static void Close(vlc_object_t *);

static const int flv_filter_mode_list[] = { 0, 1 };
static const char *const flv_filter_mode_list_text[] = { N_("stream"), N_("sequence") };
static const int flv_filter_seek_method_list[] = { 0, 1 };
static const char *const flv_filter_seek_method_list_text[] = { N_("range"), N_("nginx flv module") };

vlc_module_begin()
    set_category(CAT_INPUT)
    set_subcategory(SUBCAT_INPUT_STREAM_FILTER)
    set_description(N_("multi-parted flash video stream filter"))
    set_capability("stream_filter", 1)
    add_integer("flv-filter-mode", 0, FLV_FILTER_MODE_TEXT, FLV_FILTER_MODE_LONGTEXT, true)
        change_integer_list(flv_filter_mode_list, flv_filter_mode_list_text)
    add_integer("flv-filter-buffer-size", 524288, FLV_FILTER_BUFFER_SIZE_TEXT, FLV_FILTER_BUFFER_SIZE_LONGTEXT, true)
    add_string("flv-filter-output-file", NULL, FLV_FILTER_OUTPUT_FILE_TEXT, FLV_FILTER_OUTPUT_FILE_LONGTEXT, true)
    add_integer("flv-filter-seek-method", 0, FLV_FILTER_SEEK_METHOD_TEXT, FLV_FILTER_SEEK_METHOD_LONGTEXT, true)
        change_integer_list(flv_filter_seek_method_list, flv_filter_seek_method_list_text)
    set_callbacks(Open, Close)
vlc_module_end()

#define STREAM_STATUS_STARTED  1
#define STREAM_STATUS_ABORTED  2
#define STREAM_STATUS_ERROR    4

#pragma pack(push)
#pragma pack(1)
typedef struct _flv_file_header_t
{
    char magic[3];
    char version;
    char flag;
    char data_offset[4];            // BE
} flv_file_header_t;

typedef struct _flv_tag_header_t
{
    char flag;
    char data_size[3];              // BE
    char timestamp[4];              // BE 3 + 1 byte << 24
    char stream_id[3];
} flv_tag_header_t;
#pragma pack(pop)

typedef struct _sina_v_play_durl_t
{
    int i_order;
    uint32_t i_time;
    int i_length;
    char *psz_url;
    stream_t *p_stream;
    uint64_t i_actual_size;         // stream_Size()
    uint64_t i_skip;                // = sizeof(flv_file_header_t) + 4 + metadata tag + 4
    uint64_t i_size;                // = i_actual_size - i_skip
    uint64_t i_offset;              // = sizeof(flv_file_header_t) + 4 + psuedo metadata tag + 4 + sum(0 ~ n - 1, i_size)
    uint64_t i_bound;               // = sizeof(flv_file_header_t) + 4 + psuedo metadata tag + 4 + sum(0 ~ n, i_size)
    uint64_t i_start;
    AMFObject s_obj_onMetaData;
    AMFObjectProperty *p_prop_onMetaData_properties;
    AMFObjectProperty *p_prop_duration;
    AMFObjectProperty *p_prop_filesize;
    int i_prop_keyframes;
    AMFObjectProperty *p_prop_keyframes;
    int i_kf_count;
    AMFObjectProperty *p_prop_kf_times;
    AMFObjectProperty *p_prop_kf_offsets;
    size_t i_metadata_size;
    char *p_metadata_data;
} sina_v_play_durl_t;

struct stream_sys_t
{
    /* */
    int i_mode;
    int i_seek_method;
    /* */
    char *psz_output_file;
    int i_output_file_valid;
    int i_output_file_complete;
    int i_output_file_rfd;
    int i_output_file_wfd;
    /* */
    int i_length;
    uint64_t i_size;
    /* */
    uint64_t i_offset;
    uint64_t i_current_offset;
    /* */
    int i_nd;
    sina_v_play_durl_t *p_durls;
    int i_current_durl;
    stream_t *p_current_stream;
    char *psz_vstr;
    /* */
    size_t i_metadata_size;        // = sizeof(flv_file_header_t) + 4 + psuedo metadata tag + 4
    char *p_metadata_data;
    uint64_t *p_seek_point_data;
    size_t i_seek_point_size;
    /* */
    uint64_t i_peek_offset;
    size_t i_peek_size;
    char *p_peek_data;
    size_t i_peek_data_size;
    /* */
    int i_seek_state;
    uint64_t i_seek_offset;
    int i_audio_found;
    int i_video_found;
    /* */
    int i_status;
    vlc_thread_t sina_v_play_thread;
    size_t i_buffer_size;
    char *p_buffer_data;
    int i_buffer_start;
    int i_buffer_count;
    vlc_mutex_t buffer_lock;
    vlc_cond_t buffer_filled_cond;
    vlc_cond_t buffer_consumed_cond;
    char *p_tag_data;
    size_t i_tag_size;
    int i_tag_offset;
};

static int flv_check_header(const void *p_data);
static void flv_fix_tag(stream_sys_t *p_sys, void *p_data);

static int sina_v_play_durl_compare(const void *a, const void *b);
static int sina_v_play_durl_parse_init(stream_t *s, const char *xml_data, int xml_size);
static void sina_v_play_durl_parse_free(stream_t *s);

static int sina_v_play_flv_metadata_init(stream_t *s);

static void sina_v_play_set_status(stream_t *s, int status);
static int sina_v_play_wait_until_status(stream_t *s, int status, int infinite);

static void *flv_streaming_thread(void *param);
static void *flv_downloading_thread(void *param);

static void sina_v_play_dump(stream_t *s);

static int GenericRead0(stream_t *s, uint8_t *p_data, unsigned int i_size, int i_read);
static int GenericRead1(stream_t *s, uint8_t *p_data, unsigned int i_size, int i_read);
static int Seek0(stream_t *s, uint64_t i_position);
static int Seek1(stream_t *s, uint64_t i_position);
static int Peek(stream_t *s, const uint8_t **pp_peek, unsigned int i_peek);
static int Read(stream_t *s, void *buffer, unsigned int i_read);
static int Seek(stream_t *s, uint64_t i_position);
static int Control(stream_t *s, int i_query, va_list args);

static int flv_check_header(const void *p_data)
{
    const char *p_chars = (const char *) p_data;
    if (p_chars[0] != 'F' || p_chars[1] != 'L' || p_chars[2] != 'V')
        return -1;
    if (p_chars[3] > 4)
        return -1;
    if (AMF_DecodeInt32(p_chars + 5) < sizeof(flv_file_header_t))
        return -1;
    return 0;
}

static void flv_fix_tag(stream_sys_t *p_sys, void *p_data)
{
    flv_tag_header_t *p_tag_header;
    char *p_tag_data;

    p_tag_header = (flv_tag_header_t *) p_data;
    p_tag_data = (char *) p_data + sizeof(flv_tag_header_t);
    /* metadata tag */
    if (p_tag_header->flag == RTMP_PACKET_TYPE_INFO)
        p_tag_header->flag = 0x50;
    /* video tag */
    else if (p_tag_header->flag == RTMP_PACKET_TYPE_VIDEO)
    {
        /* AVC = 0x07 */
        if ((p_tag_data[0] & 0x07) == 0x07)
        {
            /* AVC EoS packet */
            if (p_tag_data[1] == 0x02)
            {
                p_tag_header->flag = 0x50;
            }
            /* AVC configuration packet */
            else if (p_tag_data[1] == 0x00)
            {
                if (p_sys->i_video_found)
                    p_tag_header->flag = 0x50;
                else
                    p_sys->i_video_found = -1;
            }
        }
    }
    /* audio tag */
    else if (p_tag_header->flag == RTMP_PACKET_TYPE_AUDIO)
    {
        /* AAC = 0xa0, AAC configuration packet */
        if (((p_tag_data[0] & 0xa0) == 0xa0) && (p_tag_data[1] == 0x00))
        {
            if (p_sys->i_audio_found)
            {
                /* discard this tag safely (for avformat demuxer) */
                p_tag_header->flag = 0x50;
            }
            else
                p_sys->i_audio_found = -1;
        }
    }
}

static int sina_v_play_durl_compare(const void *a, const void *b)
{
    const sina_v_play_durl_t *da = (const sina_v_play_durl_t *) a;
    const sina_v_play_durl_t *db = (const sina_v_play_durl_t *) b;
    return (da->i_order - db->i_order);
}

static int sina_v_play_durl_parse_init(stream_t *s, const char *xml_data, int xml_size)
{
    stream_sys_t *p_sys = s->p_sys;
    int result = -1;
    int i;
    sina_v_play_durl_t *durl;
    xmlDocPtr xml_doc;
    xmlXPathContextPtr xpathCtx;
    xmlXPathObjectPtr xpathObj;
    xmlNodeSetPtr xmlNodes;
    xmlNodePtr xmlNode;
    xmlChar *xmlContent;

    xml_doc = xmlReadMemory(xml_data, xml_size, NULL, NULL, 0);
    if (xml_doc)
    {

        xpathCtx = xmlXPathNewContext(xml_doc);
        if (xpathCtx)
        {
            // result
            xpathObj = xmlXPathEvalExpression((const xmlChar *) "/video/result[1]", xpathCtx);
            if (xpathObj)
            {
                xmlNodes = xpathObj->nodesetval;
                if (!xmlNodes || xmlNodes->nodeNr != 1 || !xmlNodes->nodeTab[0])
                    goto out;
                xmlNode = xmlNodes->nodeTab[0];
                if (xmlNodes->nodeTab[0]->type != XML_ELEMENT_NODE)
                    goto out;
                xmlContent = xmlNodeGetContent(xmlNode);
                if (strcmp((const char *) xmlContent, "suee"))
                    goto out;
            }
            // timelength
            xpathObj = xmlXPathEvalExpression((const xmlChar *) "/video/timelength[1]", xpathCtx);
            if (xpathObj)
            {
                xmlNodes = xpathObj->nodesetval;
                if (!xmlNodes || xmlNodes->nodeNr != 1 || !xmlNodes->nodeTab[0])
                    goto out;
                xmlNode = xmlNodes->nodeTab[0];
                if (xmlNodes->nodeTab[0]->type != XML_ELEMENT_NODE)
                    goto out;
                xmlContent = xmlNodeGetContent(xmlNode);
                p_sys->i_length = atoi((const char *) xmlContent);
            }
            // vstr
            xpathObj = xmlXPathEvalExpression((const xmlChar *) "/video/vstr", xpathCtx);
            if (xpathObj)
            {
                xmlNodes = xpathObj->nodesetval;
                if (!xmlNodes || xmlNodes->nodeNr != 1 || !xmlNodes->nodeTab[0])
                    goto out;
                xmlNode = xmlNodes->nodeTab[0];
                if (xmlNodes->nodeTab[0]->type != XML_ELEMENT_NODE)
                    goto out;
                xmlContent = xmlNodeGetContent(xmlNode);
                p_sys->psz_vstr = strdup((const char *) xmlContent);
            }
            // durl
            xpathObj = xmlXPathEvalExpression((const xmlChar *) "/video/durl", xpathCtx);
            if (xpathObj)
            {
                xmlNodePtr child;

                xmlNodes = xpathObj->nodesetval;
                if (!xmlNodes)
                    goto out;
                p_sys->i_nd = xmlNodes->nodeNr;
                p_sys->p_durls = calloc(xmlNodes->nodeNr, sizeof(sina_v_play_durl_t));
                if (!p_sys->p_durls)
                    goto out;
                for (i = 0; i < p_sys->i_nd; i++)
                {
                    xmlNode = xmlNodes->nodeTab[i];
                    durl = (sina_v_play_durl_t *)(&p_sys->p_durls[i]);
                    child = xmlNode->children;
                    if (!child)
                        goto out;
                    while (child->next)
                    {
                        xmlContent = xmlNodeGetContent(child);
                        if (!strcmp((const char *) child->name, "order"))
                            durl->i_order = atoi((const char *) xmlContent);
                        else if (!strcmp((const char *) child->name, "length"))
                            durl->i_length = atoi((const char *) xmlContent);
                        else if (!strcmp((const char *) child->name, "url"))
                        {
                            if (p_sys->psz_vstr)
                            {
                                if (asprintf(&durl->psz_url, "%s?vstr=%s", (const char *) xmlContent, p_sys->psz_vstr) < 0)
                                    goto out;
                            }
                            else
                                durl->psz_url = strdup((const char *) xmlContent);
                            if (!durl->psz_url)
                                goto out;
                        }
                        child = child->next;
                    }
                }
            }
            // everything is fine
            result = 0;
out:
            if (xpathObj)
                xmlXPathFreeObject(xpathObj);
            xmlXPathFreeContext(xpathCtx);
        }
        xmlFreeDoc(xml_doc);
    }
    xmlCleanupParser();
    if (!result)
        qsort(p_sys->p_durls, p_sys->i_nd, sizeof(sina_v_play_durl_t), sina_v_play_durl_compare);
    else
        sina_v_play_durl_parse_free(s);

    return result;
}

static void sina_v_play_durl_parse_free(stream_t *s)
{
    stream_sys_t *p_sys = s->p_sys;
    int i;
    sina_v_play_durl_t *p_durl;

    if (!p_sys)
        return;
    if (p_sys->i_nd && p_sys->p_durls)
    {
        for (i = 0; i < p_sys->i_nd; i++)
        {
            p_durl = (sina_v_play_durl_t *)(&p_sys->p_durls[i]);
            if (p_durl->psz_url)
            {
                free(p_durl->psz_url);
                p_durl->psz_url = NULL;
            }
            if (p_durl->p_metadata_data)
            {
                free(p_durl->p_metadata_data);
                p_durl->p_metadata_data = NULL;
            }
            if (p_durl->p_stream)
            {
                stream_Delete(p_durl->p_stream);
                p_durl->p_stream = NULL;
            }
        }
        if (p_sys->p_durls)
        {
            free(p_sys->p_durls);
            p_sys->p_durls = NULL;
        }
    }
    if (p_sys->psz_vstr)
    {
        free(p_sys->psz_vstr);
        p_sys->psz_vstr = NULL;
    }
}

static int sina_v_play_flv_metadata_init(stream_t *s)
{
    stream_sys_t *p_sys = s->p_sys;
    sina_v_play_durl_t *p_durl, *p_durl_0;
    flv_tag_header_t tag_header;
    flv_file_header_t file_header;
    int i_data_offset, i_read, i_last_length = 0;
    /* must be 1 + 2 + onMetaData + ECMA array */
    static char s_onMetaData[] = {0x02, 0x00, 0x0a, 0x6f, 0x6e, 0x4d, 0x65, 0x74, 0x61, 0x44, 0x61, 0x74, 0x61, 0x08};
    int i, n, x, rc;
    int i_has_kf_array, i_kf_count = 0, i_extra_kf_count = 0, i_extra_kf_size;
    int i_tag_body_size;
    AMFObjectProperty s_obj_property;
    AMFObjectProperty *p_property1, *p_property2;
    AMFObjectProperty *p_property_time, *p_property_offset;
    static char s_flv_file_header[] = {0x46, 0x4c, 0x56, 0x01, 0x05, 0x00, 0x00, 0x00, 0x09};
    char *p_encode_start, *p_encode_end, *p_encode_limit;

    /* */
    p_durl_0 = &p_sys->p_durls[0];
    /* read FLV header and metadata */
    for (i = 0; i < p_sys->i_nd; i++)
    {
        p_durl = &p_sys->p_durls[i];
        p_durl->p_stream = stream_UrlNew(s, p_durl->psz_url);
        if (!p_durl->p_stream)
        {
            msg_Err(s, "Could not create stream: #%d(`%s\').", p_durl->i_order, p_durl->psz_url);
            return -1;
        }
        /* find data offset */
        i_read = stream_Read(p_durl->p_stream, &file_header, sizeof(flv_file_header_t));
        if (i_read != sizeof(flv_file_header_t) || (flv_check_header(&file_header) < 0))
        {
            msg_Err(s, "#%d(`%s\') does not look like a FLV file.", p_durl->i_order, p_durl->psz_url);
            return -1;
        }
        i_data_offset = AMF_DecodeInt32(file_header.data_offset);
        if (i_data_offset != sizeof(flv_file_header_t))
            msg_Warn(s, "Abnormal data offset in FLV header: 0x%08x.", i_data_offset);
        /* ignore 4-byte tag size */
        if (stream_Seek(p_durl->p_stream, i_data_offset + 4) < 0)
        {
            msg_Err(s, "Could not seek #%d(`%s\') to %"PRIu64".", p_durl->i_order, p_durl->psz_url, (uint64_t)(i_data_offset + 4));
            return -1;
        }
        /* validate first tag header */
        i_read = stream_Read(p_durl->p_stream, &tag_header, sizeof(flv_tag_header_t));
        if (i_read != sizeof(flv_tag_header_t))
        {
            msg_Err(s, "Could not read stream: #%d(`%s\')@%"PRIu64" for %d bytes.", p_durl->i_order, p_durl->psz_url, stream_Tell(p_durl->p_stream), (int) sizeof(flv_tag_header_t));
            return -1;
        }
        p_durl->i_metadata_size = (tag_header.flag == RTMP_PACKET_TYPE_INFO) ? (sizeof(flv_tag_header_t) + AMF_DecodeInt24((const char *) tag_header.data_size) + 4): 0;
        if (p_durl->i_metadata_size)
        {
            p_durl->p_metadata_data = malloc(sizeof(flv_tag_header_t) + p_durl->i_metadata_size + 4);
            if (!p_durl->p_metadata_data)
            {
                msg_Err(s, "Could not allocate %d bytes for storing metadata for #%d.", (int) p_durl->i_metadata_size, p_durl->i_order);
                return -1;
            }
            memcpy(p_durl->p_metadata_data, &tag_header, sizeof(flv_tag_header_t));
            i_read = stream_Read(p_durl->p_stream, p_durl->p_metadata_data + sizeof(flv_tag_header_t), p_durl->i_metadata_size - sizeof(flv_tag_header_t));
            if (i_read < (signed)(p_durl->i_metadata_size - sizeof(flv_tag_header_t)))
            {
                msg_Err(s, "Could not read stream: #%d(`%s\')@%"PRIu64" for %d bytes.", p_durl->i_order, p_durl->psz_url, stream_Tell(p_durl->p_stream), (int)(p_durl->i_metadata_size - sizeof(flv_tag_header_t)));
                return -1;
            }
        }
        /* setup known fields */
        p_durl->i_actual_size = stream_Size(p_durl->p_stream);
        p_durl->i_skip = i_data_offset + 4;
        if (p_durl->i_metadata_size > 0)
            p_durl->i_skip += p_durl->i_metadata_size;
        p_durl->i_size = p_durl->i_actual_size - p_durl->i_skip;
        /* will be fixed later */
        p_sys->i_size += p_durl->i_size;
        if (i > 0)
            p_durl->i_time += i_last_length;
        i_last_length += p_durl->i_length;
    }
    msg_Dbg(s, "Metadata tags loaded.");
    /* rebuild FLV header and metadata tag */
    //RTMP_LogSetLevel(RTMP_LOGALL);
    i_has_kf_array = 0;
    for (i = 0; i < p_sys->i_nd; i++)
    {
        p_durl = &(p_sys->p_durls[i]);
        /* sanity check */
        if ((unsigned) p_durl->i_metadata_size < (sizeof(flv_tag_header_t) + sizeof(s_onMetaData) + 4))
        {
            if (i > 0)
                continue;
            msg_Err(s, "#%d(`%s\') must contain valid metadata.", p_durl->i_order, p_durl->psz_url);
            return -1;
        }
        if (memcmp(s_onMetaData, p_durl->p_metadata_data + sizeof(flv_tag_header_t), sizeof(s_onMetaData)))
        {
            if (i > 0)
                continue;
            msg_Err(s, "Could not find onMetaData in #%d(`%s\').", p_durl->i_order, p_durl->psz_url);
            return -1;
        }
        /* librtmp needs to ignore 1st type byte to work */
        rc = AMF_Decode(&p_durl->s_obj_onMetaData, p_durl->p_metadata_data + sizeof(flv_tag_header_t) + 1, p_durl->i_metadata_size - (sizeof(flv_tag_header_t) + 1 + 4), true);
        if (rc < 0)
        {
            /* #1 must not fail */
            if (i > 0)
                continue;
            msg_Err(s, "AMF_Decode on #%d(`%s\') failed.", p_durl->i_order, p_durl->psz_url);
            return -1;
        }
        //AMF_Dump(&p_durl->s_obj_onMetaData);
        /* { "onMetaData": {...} } */
        p_durl->p_prop_onMetaData_properties = AMF_GetProp(&p_durl->s_obj_onMetaData, NULL, 0);
        if (p_durl->p_prop_onMetaData_properties->p_type != AMF_ECMA_ARRAY)
            continue;
        /* avcodec only concerns {duration:n,keyframes:{times:{},filepositions:{}}} */
        for (n = 0; n < AMF_CountProp(&p_durl->p_prop_onMetaData_properties->p_vu.p_object); n++)
        {
            p_property1 = AMF_GetProp(&p_durl->p_prop_onMetaData_properties->p_vu.p_object, NULL, n);
            if ((p_property1->p_type == AMF_OBJECT) && (p_property1->p_name.av_len == 9) && !memcmp("keyframes", p_property1->p_name.av_val, 9))
            {
                p_durl->i_prop_keyframes = n;
                p_durl->p_prop_keyframes = p_property1;
                i_has_kf_array += 1;
                for (x = 0; x < AMF_CountProp(&p_property1->p_vu.p_object); x++)
                {
                    p_property2 = AMF_GetProp(&p_property1->p_vu.p_object, NULL, x);
                    /* it was AMF_OBJECT for ECMA/STRICT array, so patched librtmp */
                    if ((p_property2->p_type == AMF_STRICT_ARRAY) && (p_property2->p_name.av_len == 13) && !memcmp("filepositions", p_property2->p_name.av_val, 13))
                    {
                        p_durl->p_prop_kf_offsets = p_property2;
                        p_durl->i_kf_count = AMF_CountProp(&p_property2->p_vu.p_object);
                    }
                    if ((p_property2->p_type == AMF_STRICT_ARRAY) && (p_property2->p_name.av_len == 5) && !memcmp("times", p_property2->p_name.av_val, 5))
                    {
                        p_durl->p_prop_kf_times = p_property2;
                        /* assumed */
                        p_durl->i_kf_count = AMF_CountProp(&p_property2->p_vu.p_object);
                    }
                }
            }
            if ((p_property1->p_type == AMF_NUMBER) && (p_property1->p_name.av_len == 8) && !memcmp("duration", p_property1->p_name.av_val, 8))
                p_durl->p_prop_duration = p_property1;
            if ((p_property1->p_type == AMF_NUMBER) && (p_property1->p_name.av_len == 8) && !memcmp("filesize", p_property1->p_name.av_val, 8))
                p_durl->p_prop_filesize = p_property1;
        }
        i_kf_count += p_durl->i_kf_count;
        if (i > 0)
            i_extra_kf_count += p_durl->i_kf_count;
    }
    /* process keyframes object */
    if (i_has_kf_array == p_sys->i_nd)
    {
        /* enlarge metadata */
        i_extra_kf_size = i_extra_kf_count * 2 * (1 + sizeof(double));
        p_durl_0->i_size = p_durl_0->i_actual_size + i_extra_kf_size;
        p_durl_0->i_bound = p_durl_0->i_size;
        /* merge and correct */
        for (i = 1; i < p_sys->i_nd; i++)
        {
            p_durl = &p_sys->p_durls[i];
            p_durl->i_offset = p_sys->p_durls[i - 1].i_bound;
            p_durl->i_bound = p_durl->i_offset + p_durl->i_size;
            for (n = 0; n < p_durl->i_kf_count; n++)
            {
                s_obj_property.p_name.av_val = NULL;
                s_obj_property.p_name.av_len = 0;
                s_obj_property.p_type = AMF_NUMBER;
                p_property_offset = AMF_GetProp(&p_durl->p_prop_kf_offsets->p_vu.p_object, NULL, n);
                s_obj_property.p_vu.p_number = (double)(p_durl->i_offset - p_durl->i_skip) + p_property_offset->p_vu.p_number;
                AMF_AddProp(&p_durl_0->p_prop_kf_offsets->p_vu.p_object, &s_obj_property);
                p_property_time = AMF_GetProp(&p_durl->p_prop_kf_times->p_vu.p_object, NULL, n);
                s_obj_property.p_vu.p_number = (double)(p_durl->i_time) / 1000.0 + p_property_time->p_vu.p_number;
                AMF_AddProp(&p_durl_0->p_prop_kf_times->p_vu.p_object, &s_obj_property);
            }
        }
        msg_Dbg(s, "Injected %d elements into keyframes.", i_extra_kf_count);
    }
    else
    {
        /* discard keyframes, lose seeking capability? */
        i_extra_kf_size = 0;
        if (p_durl_0->p_prop_keyframes)
        {
            /* truncate metadata */
            if (p_durl_0->p_prop_kf_offsets)
                i_extra_kf_size -= (2 + 13 + 4 + (AMF_CountProp(&p_durl_0->p_prop_kf_offsets->p_vu.p_object)) * (1 + sizeof(double)));
            if (p_durl_0->p_prop_kf_times)
                i_extra_kf_size -= (2 + 5 + 4 + (AMF_CountProp(&p_durl_0->p_prop_kf_times->p_vu.p_object)) * (1 + sizeof(double)));
            /* release keyframes in onMetaData */
            AMFProp_Reset(p_durl_0->p_prop_keyframes);
            if (p_durl_0->p_prop_onMetaData_properties->p_vu.p_object.o_num - p_durl_0->i_prop_keyframes > 1)
                memmove(p_durl_0->p_prop_onMetaData_properties->p_vu.p_object.o_props + p_durl_0->i_prop_keyframes, p_durl_0->p_prop_onMetaData_properties->p_vu.p_object.o_props + p_durl_0->i_prop_keyframes + 1, (p_durl_0->p_prop_onMetaData_properties->p_vu.p_object.o_num - p_durl_0->i_prop_keyframes - 1) * sizeof(AMFObjectProperty));
            p_durl_0->p_prop_onMetaData_properties->p_vu.p_object.o_num -= 1;
        }
        msg_Dbg(s, "Discarded keyframes.");
    }
    /* fix filesize */
    p_sys->i_size += (p_durl_0->i_skip + i_extra_kf_size);
    if (p_durl_0->p_prop_filesize)
        p_durl_0->p_prop_filesize->p_vu.p_number = (double) p_sys->i_size;
    else
    {
        s_obj_property.p_name.av_val = (char *) "filesize";
        s_obj_property.p_name.av_len = 8;
        s_obj_property.p_type = AMF_NUMBER;
        s_obj_property.p_vu.p_number = (double) p_sys->i_size;
        AMF_AddProp(&p_durl_0->s_obj_onMetaData, &s_obj_property);
    }
    /* fix duration */
    if (p_durl_0->p_prop_duration)
        p_durl_0->p_prop_duration->p_vu.p_number = (double) p_sys->i_length / 1000.0;
    else
    {
        s_obj_property.p_name.av_val = (char *) "duration";
        s_obj_property.p_name.av_len = 8;
        s_obj_property.p_type = AMF_NUMBER;
        s_obj_property.p_vu.p_number = (double) p_sys->i_length / 1000.0;
        AMF_AddProp(&p_durl_0->s_obj_onMetaData, &s_obj_property);
    }
    /* initialize seek points */
    if (i_kf_count > 0)
    {
        /* prepare to save seek points */
        p_sys->p_seek_point_data = calloc(i_kf_count, sizeof(uint64_t));
        if (!p_sys->p_seek_point_data && !p_sys->i_mode)
            msg_Warn(s, "Seeking will be disabled.");
        else
            p_sys->i_seek_point_size = i_kf_count;
        for (n = 0; n < AMF_CountProp(&p_durl_0->p_prop_kf_offsets->p_vu.p_object); n++)
        {
            p_property_offset = AMF_GetProp(&p_durl_0->p_prop_kf_offsets->p_vu.p_object, NULL, n);
            /* offsets needs to be corrected in #1 */
            if ((i_extra_kf_size) && (n < p_durl_0->i_kf_count))
                p_property_offset->p_vu.p_number += (double) i_extra_kf_size;
            if(p_sys->p_seek_point_data)
                p_sys->p_seek_point_data[n] = (uint64_t) p_property_offset->p_vu.p_number;
        }
    }
    //AMF_Dump(&p_durl_0->s_obj_onMetaData);
    /*  */
    p_sys->i_metadata_size = sizeof(flv_file_header_t) + 4 + p_durl_0->i_metadata_size + i_extra_kf_size;
    p_sys->p_metadata_data = malloc(p_sys->i_metadata_size);
    if (!p_sys->p_metadata_data)
    {
        msg_Err(s, "Could not allocate %zd bytes for storing psuedo metadata.", p_sys->i_metadata_size);
        return -1;
    }
    p_encode_start = p_sys->p_metadata_data;
    /* write FLV header */
    memcpy(p_encode_start, s_flv_file_header, sizeof(flv_file_header_t));
    p_encode_start += sizeof(flv_file_header_t);
    /* write tag0 size */
    *((uint32_t *) p_encode_start) = 0;
    p_encode_start += 4;
    /* write tag header: flags */
    *p_encode_start++ = RTMP_PACKET_TYPE_INFO;
    /* write tag header: data size */
    i_tag_body_size = p_durl_0->i_metadata_size + i_extra_kf_size - sizeof(flv_tag_header_t) - 4;
    AMF_EncodeInt24(p_encode_start, p_encode_start + 3, i_tag_body_size);
    /* write tag header: timestamp */
    p_encode_start += 3;
    *((uint32_t *) p_encode_start) = 0;
    p_encode_start += 4;
    /* write tag header: stream id */
    AMF_EncodeInt24(p_encode_start, p_encode_start + 3, 0);
    p_encode_start += 3;
    /* write the tag */
    *p_encode_start++ = AMF_STRING;
    p_encode_limit = (p_sys->p_metadata_data + p_sys->i_metadata_size);
    p_encode_end = AMFProp_Encode(&p_durl_0->s_obj_onMetaData.o_props[0], p_encode_start, p_encode_limit);
    /* clean AMF stuffs */
    for (i = 0; i < p_sys->i_nd; i++)
    {
        p_durl = &(p_sys->p_durls[i]);
        if (p_durl->p_metadata_data)
        {
            if (p_durl->s_obj_onMetaData.o_props)
                AMF_Reset(&p_durl->s_obj_onMetaData);
            free(p_durl->p_metadata_data);
            p_durl->p_metadata_data = NULL;
        }
    }
    /* what? */
    if (p_encode_end == NULL)
    {
        msg_Err(s, "AMF_Encode failed.");
        return -1;
    }
    /* write previous tag size */
    AMF_EncodeInt32(p_sys->p_metadata_data + p_sys->i_metadata_size - 4, p_sys->p_metadata_data + p_sys->i_metadata_size, sizeof(flv_tag_header_t) + i_tag_body_size);

    msg_Dbg(s, "Psuedo metadata created.");

    return 0;
}

static void sina_v_play_set_status(stream_t *s, int status)
{
    stream_sys_t *p_sys = s->p_sys;

    vlc_mutex_lock(&p_sys->buffer_lock);
    p_sys->i_status |= status;
    vlc_cond_signal(&p_sys->buffer_filled_cond);
    vlc_mutex_unlock(&p_sys->buffer_lock);
}

static int sina_v_play_wait_until_status(stream_t *s, int status, int infinite)
{
    stream_sys_t *p_sys = s->p_sys;
    int ret = 0;

    vlc_mutex_lock(&p_sys->buffer_lock);
    while (!(p_sys->i_status & status))
    {
        vlc_cond_wait(&p_sys->buffer_filled_cond, &p_sys->buffer_lock);
        ret = p_sys->i_status;
        if (!infinite)
            break;
    }
    vlc_mutex_unlock(&p_sys->buffer_lock);

    return ret;
}

static void *flv_streaming_thread(void *param)
{
    stream_t *s = (stream_t *) param;
    stream_sys_t *p_sys = s->p_sys;
    char *psz_url;
    sina_v_play_durl_t *p_durl = NULL;
    uint64_t i_offset, i_test_offset;
    int i_read, i_buffer_end, i_tag_timestamp;
    size_t n, i_left, i_tag_size;
    flv_tag_header_t tag_header;
    flv_tag_header_t *p_tag_header = NULL;
    void *p_tag_tmp;

    assert(p_sys->i_mode == 0);
    msg_Dbg(s, "%s thread started.", flv_filter_mode_list_text[p_sys->i_mode]);
    /* minium reading unit is a FLV tag */
    if (sina_v_play_flv_metadata_init(s) < 0)
    {
        msg_Err(s, "Could not create pseudo metadata.");
        sina_v_play_set_status(s, STREAM_STATUS_ERROR);
        goto bail;
    }
    /* initial status */
    memcpy(p_sys->p_buffer_data, p_sys->p_metadata_data, p_sys->i_metadata_size);
    p_sys->i_current_offset = p_sys->i_metadata_size;
    vlc_mutex_lock(&p_sys->buffer_lock);
    p_sys->i_buffer_start = 0;
    p_sys->i_buffer_count = p_sys->i_metadata_size;
    vlc_cond_signal(&p_sys->buffer_filled_cond);
    vlc_mutex_unlock(&p_sys->buffer_lock);
    /* */
    sina_v_play_set_status(s, STREAM_STATUS_STARTED);
    sina_v_play_dump(s);
    /* main loop */
    while (-1)
    {
        /* requested to seek? */
        if (p_sys->i_seek_state == -1)
        {
            msg_Dbg(s, "Seek to %"PRIu64".", p_sys->i_seek_offset);
            /* not seekable */
            if (!p_sys->i_seek_point_size)
            {
                msg_Warn(s, "Seeking is not possible.");
                vlc_mutex_lock(&p_sys->buffer_lock);
                p_sys->i_seek_state = 0;
                vlc_cond_signal(&p_sys->buffer_filled_cond);
                vlc_mutex_unlock(&p_sys->buffer_lock);
            }
            else
            {
                /* seek point must be 0, i_metadata_size, keyframes... */
                if (p_sys->i_seek_offset < p_sys->i_metadata_size)
                    p_sys->i_current_offset = 0;
                else if (p_sys->i_seek_offset < p_sys->p_seek_point_data[0])
                    p_sys->i_current_offset = p_sys->i_metadata_size;
                else
                {
                    /* find a seek point */
                    for (n = 1; n < p_sys->i_seek_point_size; n++)
                    {
                        if (p_sys->i_seek_offset < p_sys->p_seek_point_data[n])
                            break;
                    }
                    p_sys->i_current_offset = p_sys->p_seek_point_data[n - 1];
                }
                p_sys->i_seek_state = 1;
                msg_Dbg(s, "Seeking will start at %"PRIu64".", p_sys->i_current_offset);
            }
        }
        /* where to start */
        for (p_sys->i_current_durl = 0; p_sys->i_current_durl < p_sys->i_nd; p_sys->i_current_durl++)
        {
            p_durl = &p_sys->p_durls[p_sys->i_current_durl];
            if (p_sys->i_current_offset < p_durl->i_bound)
                break;
        }
        if (p_sys->i_current_durl == p_sys->i_nd)
        {
            /* at the end, wait for aborting or seeking */
            msg_Dbg(s, "EOF reached, wait until aborting or seeking is requested.");
            vlc_mutex_lock(&p_sys->buffer_lock);
            while (!(p_sys->i_status & STREAM_STATUS_ABORTED) && (!p_sys->i_seek_state))
                vlc_cond_wait(&p_sys->buffer_consumed_cond, &p_sys->buffer_lock);
            vlc_mutex_unlock(&p_sys->buffer_lock);
            if (p_sys->i_status & STREAM_STATUS_ABORTED)
                break;
            else
                continue;
        }
        if (p_sys->i_current_offset >= p_sys->i_metadata_size)
        {
            /* make sure starting at the right place after seeking */
            if (p_sys->i_current_durl > 0)
                i_offset = p_durl->i_skip + p_sys->i_current_offset - p_durl->i_offset;
            else
                i_offset = p_durl->i_skip + p_sys->i_current_offset - p_sys->i_metadata_size;
            i_test_offset = (unsigned) stream_Tell(p_durl->p_stream);
            if ((p_sys->i_seek_method == 1) && (p_durl->i_start > 0))
                i_test_offset += (p_durl->i_start - sizeof(flv_file_header_t) - 4);
            if (i_test_offset != i_offset)
            {
                msg_Dbg(s, "Seek #%d(`%s\') to offset %"PRIu64"(%"PRIu64").", p_durl->i_order, p_durl->psz_url, i_offset, i_test_offset);
                if (p_sys->i_seek_method == 0)
                {
                    if (stream_Seek(p_durl->p_stream, i_offset) < 0)
                    {
                        msg_Err(s, "Could not seek #%d(`%s\') to %"PRIu64".", p_durl->i_order, p_durl->psz_url, i_offset);
                        sina_v_play_set_status(s, STREAM_STATUS_ERROR);
                        break;
                    }
                }
                else
                {
                    /* nginx/httpd flv module */
                    stream_Delete(p_durl->p_stream);
                    if (asprintf(&psz_url, p_sys->psz_vstr ? "%s&start=%"PRIu64 : "%s?start=%"PRIu64, p_durl->psz_url, i_offset) < 0)
                    {
                        msg_Err(s, "Could not make URL(`%s?start=%"PRIu64"').", p_durl->psz_url, i_offset);
                        sina_v_play_set_status(s, STREAM_STATUS_ERROR);
                        break;
                    }
                    p_durl->p_stream = stream_UrlNew(s, psz_url);
                    free(psz_url);
                    if (!p_durl->p_stream)
                    {
                        msg_Err(s, "Could not create stream for `%s?start=%"PRIu64"'.", p_durl->psz_url, i_offset);
                        sina_v_play_set_status(s, STREAM_STATUS_ERROR);
                        break;
                    }
                    i_read = stream_Read(p_durl->p_stream, NULL, sizeof(flv_file_header_t) + 4);
                    if (i_read != sizeof(flv_file_header_t) + 4)
                    {
                        msg_Err(s, "Oops!");
                        sina_v_play_set_status(s, STREAM_STATUS_ERROR);
                        break;
                    }
                    p_durl->i_start = i_offset;
                }
            }
            p_sys->p_current_stream = p_durl->p_stream;
            /* get the underlying tag size */
            i_read = stream_Read(p_sys->p_current_stream, &tag_header, sizeof(flv_tag_header_t));
            if (i_read != sizeof(flv_tag_header_t))
            {
                msg_Err(s, "Could not read #%d(`%s\')@%"PRIu64" for %d bytes.", p_durl->i_order, p_durl->psz_url, stream_Tell(p_sys->p_current_stream), (int) sizeof(flv_tag_header_t));
                sina_v_play_set_status(s, STREAM_STATUS_ERROR);
                break;
            }
            p_tag_header = &tag_header;
            i_tag_size = sizeof(flv_tag_header_t) + AMF_DecodeInt24((const char *) p_tag_header->data_size) + 4;
            /* whether something wrong here or possible corrupted data */
            if (i_tag_size > p_sys->i_buffer_size)
            {
                msg_Err(s, "Bad FLV tag size: %zd, #%d(`%s\')@%"PRIu64" virtual offset = %"PRIu64".", i_tag_size, p_durl->i_order, p_durl->psz_url, stream_Tell(p_sys->p_current_stream), p_sys->i_current_offset);
                sina_v_play_set_status(s, STREAM_STATUS_ERROR);
                break;
            }
            /* fix timestamp in the tag header if necessary */
            if (p_sys->i_current_durl > 0)
            {
                i_tag_timestamp = AMF_DecodeInt24(p_tag_header->timestamp) | (p_tag_header->timestamp[3] << 24);
                i_tag_timestamp += p_durl->i_time;
                AMF_EncodeInt24(p_tag_header->timestamp, p_tag_header->timestamp + 3, i_tag_timestamp);
                p_tag_header->timestamp[3] = (i_tag_timestamp & 0xff000000) >> 24;
            }
        }
        else
            i_tag_size = p_sys->i_metadata_size;
        /* allocate a tag */
        if (p_sys->i_tag_size < i_tag_size)
        {
            p_tag_tmp = realloc(p_sys->p_tag_data, i_tag_size);
            if (!p_tag_tmp)
            {
                msg_Err(s, "Could not allocate %d for a FLV tag.", (int) i_tag_size);
                sina_v_play_set_status(s, STREAM_STATUS_ERROR);
                break;
            }
            p_sys->p_tag_data = p_tag_tmp;
        }
        if (p_sys->i_current_offset >= p_sys->i_metadata_size)
            memcpy(p_sys->p_tag_data, p_tag_header, sizeof(flv_tag_header_t));
        else
            memcpy(p_sys->p_tag_data, p_sys->p_metadata_data, p_sys->i_metadata_size);
        p_sys->i_tag_size = i_tag_size;
        p_sys->i_tag_offset = 0;
        /* seeking is almost completed */
        if (p_sys->i_seek_state)
        {
            if ((p_sys->i_seek_offset >= p_sys->i_current_offset) && (p_sys->i_seek_offset < p_sys->i_current_offset + p_sys->i_tag_size))
            {
                p_sys->i_tag_offset = (int)(p_sys->i_seek_offset - p_sys->i_current_offset);
                p_sys->i_seek_state = 2;
                msg_Dbg(s, "Seeking is almost completed(seek offset = %"PRIu64", curr offset = %"PRIu64", tag size = %zd).", p_sys->i_seek_offset, p_sys->i_current_offset, p_sys->i_tag_size);
            }
        }
        /* wait the buffer to be consumed */
        vlc_mutex_lock(&p_sys->buffer_lock);
        while ((p_sys->i_seek_state == 0) && (p_sys->i_buffer_count + i_tag_size > p_sys->i_buffer_size))
        {
            vlc_cond_wait(&p_sys->buffer_consumed_cond, &p_sys->buffer_lock);
            if ((p_sys->i_seek_state == -1) || (p_sys->i_status & STREAM_STATUS_ABORTED))
                break;
        }
        vlc_mutex_unlock(&p_sys->buffer_lock);
        /* aborting is requested */
        if (p_sys->i_status & STREAM_STATUS_ABORTED)
            break;
        /* seeking is requested */
        if (p_sys->i_seek_state == -1)
            continue;
        /* read a tag */
        if (p_sys->i_current_offset >= p_sys->i_metadata_size)
        {
            i_read = stream_Read(p_sys->p_current_stream, p_sys->p_tag_data + sizeof(flv_tag_header_t), i_tag_size - sizeof(flv_tag_header_t));
            if ((unsigned) i_read != (i_tag_size - sizeof(flv_tag_header_t)))
            {
                msg_Err(s, "Error reading #%d(`%s\')@%"PRIu64", %d/%d, virtual offset = %"PRIu64".", p_durl->i_order, p_durl->psz_url, stream_Tell(p_sys->p_current_stream), i_read, (int)(i_tag_size - sizeof(flv_tag_header_t)), p_sys->i_current_offset);
                sina_v_play_set_status(s, STREAM_STATUS_ERROR);
                break;
            }
        }
        vlc_mutex_lock(&p_sys->buffer_lock);
        /* need to process the tag */
        if ((p_sys->i_seek_state == 0) || (p_sys->i_seek_state == 2))
        {
            /* special case for AVC/AAC */
            flv_fix_tag(p_sys, p_sys->p_tag_data);
            /* copy the tag to the buffer */
            i_buffer_end = (p_sys->i_buffer_start + p_sys->i_buffer_count) % p_sys->i_buffer_size;
            if (p_sys->i_buffer_start <= i_buffer_end)
            {
                i_left = p_sys->i_buffer_size - i_buffer_end;
                if (i_left >= i_tag_size)
                    memcpy(p_sys->p_buffer_data + i_buffer_end, p_sys->p_tag_data, p_sys->i_tag_size);
                else
                {
                    memcpy(p_sys->p_buffer_data + i_buffer_end, p_sys->p_tag_data, i_left);
                    memcpy(p_sys->p_buffer_data, p_sys->p_tag_data + i_left, p_sys->i_tag_size - i_left);
                }
            }
            else
                memcpy(p_sys->p_buffer_data + i_buffer_end, p_sys->p_tag_data, p_sys->i_tag_size);
        }
        /* notify that some data is available now */
        if (!p_sys->i_seek_state)
        {
            p_sys->i_buffer_count += p_sys->i_tag_size;
            vlc_cond_signal(&p_sys->buffer_filled_cond);
        }
        /* seeking is done so notify the waiting thread */
        else if (p_sys->i_seek_state == 2)
        {
            p_sys->i_buffer_start += p_sys->i_buffer_count;
            p_sys->i_buffer_start += p_sys->i_tag_offset;
            p_sys->i_buffer_start %= p_sys->i_buffer_size;
            p_sys->i_buffer_count = p_sys->i_tag_size - p_sys->i_tag_offset;
            p_sys->i_seek_state = 0;
            p_sys->i_offset = p_sys->i_seek_offset;
            vlc_cond_signal(&p_sys->buffer_filled_cond);
            msg_Dbg(s, "Seeking is completed.");
        }
        vlc_mutex_unlock(&p_sys->buffer_lock);
        /* by tag */
        p_sys->i_current_offset += p_sys->i_tag_size;
    }

bail:
    msg_Dbg(s, "%s thread exited.", flv_filter_mode_list_text[p_sys->i_mode]);

    return NULL;
}

static void *flv_downloading_thread(void *param)
{
    stream_t *s = (stream_t *) param;
    stream_sys_t *p_sys = s->p_sys;
    sina_v_play_durl_t *p_durl = NULL;
    int rc, i_read, i_tag_timestamp;
    size_t n, tmp, i_tag_size;
    flv_tag_header_t tag_header;
    flv_tag_header_t *p_tag_header = NULL;
    void *p_tag_tmp;
    struct stat s_stat_of, *p_stat_of = &s_stat_of;

    assert(p_sys->i_mode == 1);
    msg_Dbg(s, "%s thread started.", flv_filter_mode_list_text[p_sys->i_mode]);
    /* orz, for incomplete files read/write by tag */
    if (sina_v_play_flv_metadata_init(s) < 0)
    {
        msg_Err(s, "Could not create pseudo metadata.");
        sina_v_play_set_status(s, STREAM_STATUS_ERROR);
        goto bail;
    }
    /* minium download unit is a FLV file */
    rc = fstat(p_sys->i_output_file_rfd, p_stat_of);
    if (rc < 0)
    {
        msg_Err(s, "Error occured: (%m).");
        sina_v_play_set_status(s, STREAM_STATUS_ERROR);
        goto bail;
    }
    /* do not download repeatly */
    p_sys->i_current_offset = p_stat_of->st_size;
    for (n = 0; n < (unsigned) p_sys->i_nd; n++)
    {
        p_durl = &p_sys->p_durls[n];
        if ((p_sys->i_current_offset >= p_durl->i_offset) && (p_sys->i_current_offset < p_durl->i_bound))
        {
            p_sys->i_current_offset = p_durl->i_offset;
            break;
        }
    }
    rc = lseek(p_sys->i_output_file_wfd, p_sys->i_current_offset, SEEK_SET);
    if (rc < 0)
    {
        msg_Err(s, "Error occured: (%m).");
        sina_v_play_set_status(s, STREAM_STATUS_ERROR);
        goto bail;
    }
    /* */
    sina_v_play_set_status(s, STREAM_STATUS_STARTED);
    sina_v_play_dump(s);
    /* main loop */
    while (-1)
    {
        /* where to start */
        for (p_sys->i_current_durl = 0; p_sys->i_current_durl < p_sys->i_nd; p_sys->i_current_durl++)
        {
            p_durl = &p_sys->p_durls[p_sys->i_current_durl];
            if (p_sys->i_current_offset < p_durl->i_bound)
                break;
        }
        if (p_sys->i_current_durl == p_sys->i_nd)
        {
            /* everything is done, exit now */
            fsync(p_sys->i_output_file_wfd);
            close(p_sys->i_output_file_wfd);
            break;
        }
        /* aborted? */
        if (p_sys->i_status & STREAM_STATUS_ABORTED)
            break;
        /* read FLV tag header */
        if (p_sys->i_current_offset >= p_sys->i_metadata_size)
        {
            p_sys->p_current_stream = p_durl->p_stream;
            /* get the underlying tag size */
            i_read = stream_Read(p_sys->p_current_stream, &tag_header, sizeof(flv_tag_header_t));
            if (i_read != sizeof(flv_tag_header_t))
            {
                msg_Err(s, "Could not read #%d(`%s\')@%"PRIu64" for %d bytes.", p_durl->i_order, p_durl->psz_url, stream_Tell(p_sys->p_current_stream), (int) sizeof(flv_tag_header_t));
                sina_v_play_set_status(s, STREAM_STATUS_ERROR);
                break;
            }
            p_tag_header = &tag_header;
            i_tag_size = sizeof(flv_tag_header_t) + AMF_DecodeInt24((const char *) p_tag_header->data_size) + 4;
            /* whether something wrong here or possible corrupted data */
            if (i_tag_size > p_durl->i_bound - p_sys->i_current_offset)
            {
                msg_Err(s, "Bad FLV tag size: %zd, #%d(`%s\')@%"PRIu64" virtual offset = %"PRIu64".", i_tag_size, p_durl->i_order, p_durl->psz_url, stream_Tell(p_sys->p_current_stream), p_sys->i_current_offset);
                sina_v_play_set_status(s, STREAM_STATUS_ERROR);
                break;
            }
            /* fix timestamp in the tag header if necessary */
            if (p_sys->i_current_durl > 0)
            {
                i_tag_timestamp = AMF_DecodeInt24(p_tag_header->timestamp) | (p_tag_header->timestamp[3] << 24);
                i_tag_timestamp += p_durl->i_time;
                AMF_EncodeInt24(p_tag_header->timestamp, p_tag_header->timestamp + 3, i_tag_timestamp);
                p_tag_header->timestamp[3] = (i_tag_timestamp & 0xff000000) >> 24;
            }
        }
        else
            i_tag_size = p_sys->i_metadata_size;
        /* allocate a tag */
        if (p_sys->i_tag_size < i_tag_size)
        {
            p_tag_tmp = realloc(p_sys->p_tag_data, i_tag_size);
            if (!p_tag_tmp)
            {
                msg_Err(s, "Could not allocate %d for a FLV tag.", (int) i_tag_size);
                sina_v_play_set_status(s, STREAM_STATUS_ERROR);
                break;
            }
            p_sys->p_tag_data = p_tag_tmp;
        }
        if (p_sys->i_current_offset >= p_sys->i_metadata_size)
            memcpy(p_sys->p_tag_data, p_tag_header, sizeof(flv_tag_header_t));
        else
            memcpy(p_sys->p_tag_data, p_sys->p_metadata_data, p_sys->i_metadata_size);
        p_sys->i_tag_size = i_tag_size;
        p_sys->i_tag_offset = 0;
        /* read FLV tag body */
        if (p_sys->i_current_offset >= p_sys->i_metadata_size)
        {
            i_read = stream_Read(p_sys->p_current_stream, p_sys->p_tag_data + sizeof(flv_tag_header_t), i_tag_size - sizeof(flv_tag_header_t));
            if ((unsigned) i_read != (i_tag_size - sizeof(flv_tag_header_t)))
            {
                msg_Err(s, "Error reading #%d(`%s\')@%"PRIu64", %d/%d, virtual offset = %"PRIu64".", p_durl->i_order, p_durl->psz_url, stream_Tell(p_sys->p_current_stream), i_read, (int)(i_tag_size - sizeof(flv_tag_header_t)), p_sys->i_current_offset);
                sina_v_play_set_status(s, STREAM_STATUS_ERROR);
                break;
            }
        }
        /* special case for AVC/AAC */
        flv_fix_tag(p_sys, p_sys->p_tag_data);
        /* append this tag to the output file */
        n = 0;
        while (n < p_sys->i_tag_size)
        {
            tmp = write(p_sys->i_output_file_wfd, p_sys->p_tag_data + n, p_sys->i_tag_size - n);
            if (tmp <= 0)
                break;
            n += tmp;
        }
        if (n < p_sys->i_tag_size)
        {
            msg_Err(s, "Error occured: (%m).");
            sina_v_play_set_status(s, STREAM_STATUS_ERROR);
            break;
        }
        /* notify that some data is available now */
        vlc_mutex_lock(&p_sys->buffer_lock);
        p_sys->i_current_offset += p_sys->i_tag_size;
        vlc_cond_signal(&p_sys->buffer_filled_cond);
        vlc_mutex_unlock(&p_sys->buffer_lock);
    }

bail:
    msg_Dbg(s, "%s thread exited.", flv_filter_mode_list_text[p_sys->i_mode]);

    return NULL;
}

static void sina_v_play_dump(stream_t *s)
{
    stream_sys_t *p_sys = s->p_sys;
    int i;
    sina_v_play_durl_t *p_durl;

    if (!p_sys)
        return;
    for (i = 0; i < p_sys->i_nd; i++)
    {
        p_durl = (sina_v_play_durl_t *)(&p_sys->p_durls[i]);
        msg_Dbg(s, "%02d. %s", p_durl->i_order, p_durl->psz_url);
        if (p_sys->i_status)
        {
            msg_Dbg(s, "    skip = %"PRIu64", size = %"PRIu64"(%"PRIu64" ~ %"PRIu64"), %d ~ %d", p_durl->i_skip, p_durl->i_size, p_durl->i_offset, p_durl->i_bound, p_durl->i_time, p_durl->i_time + p_durl->i_length);
            msg_Dbg(s, "    keyframes = %d", (int) p_durl->i_kf_count);
        }
    }
}

static int init_buffer(stream_t *s)
{
    stream_sys_t *p_sys = s->p_sys;

    p_sys->i_buffer_size = var_InheritInteger(s, "flv-filter-buffer-size");
    p_sys->p_buffer_data = malloc(p_sys->i_buffer_size);
    if (!p_sys->p_buffer_data)
        return VLC_EGENERIC;
    p_sys->i_buffer_start = 0;
    p_sys->i_buffer_count = 0;

    return 0;
}

static int test_output_file(stream_t *s)
{
    stream_sys_t *p_sys = s->p_sys;
    int rc;
    size_t n, tmp;
    struct stat s_stat_buffer;
    uint32_t i_data_offset;
    flv_tag_header_t s_tag_header, *p_tag_header = &s_tag_header;
    AMFObject s_obj_onMetaData = {0, NULL};
    AMFObjectProperty *p_property, *p_prop_onMetaData_properties;

    /* */
    p_sys->psz_output_file = var_InheritString(s, "flv-filter-output-file");
    if (!p_sys->psz_output_file)
        return -1;
    /* simple tests on the file */
    rc = stat(p_sys->psz_output_file, &s_stat_buffer);
    if (rc < 0)
        return -1;
    if(s_stat_buffer.st_size < 24)
        return -1;
    p_sys->i_output_file_rfd = open(p_sys->psz_output_file, O_RDONLY);
    if (p_sys->i_output_file_rfd < 0)
        return -1;
    /* read data offset */
    rc = lseek(p_sys->i_output_file_rfd, 5, SEEK_SET);
    if (rc < 0)
        goto bail;
    n = read(p_sys->i_output_file_rfd, &i_data_offset, 4);
    if (n != 4)
        goto bail;
    i_data_offset = AMF_DecodeInt32((char *) &i_data_offset);
    /* go to data offset */
    rc = lseek(p_sys->i_output_file_rfd, i_data_offset + 4, SEEK_SET);
    if (rc < 0)
        goto bail;
    /* read tag header */
    n = read(p_sys->i_output_file_rfd, p_tag_header, sizeof(flv_tag_header_t));
    if ((n != sizeof(flv_tag_header_t)) || (p_tag_header->flag != RTMP_PACKET_TYPE_INFO))
        goto bail;
    p_sys->i_metadata_size = AMF_DecodeInt24((char *) &p_tag_header->data_size);
    p_sys->p_metadata_data = malloc(p_sys->i_metadata_size);
    if (!p_sys->p_metadata_data)
        goto bail;
    /* read tag body */
    n = 0;
    while (n < p_sys->i_metadata_size)
    {
        tmp = read(p_sys->i_output_file_rfd, p_sys->p_metadata_data + n, p_sys->i_metadata_size - n);
        if (tmp <= 0)
            break;
        n += tmp;
     }
    if (n != p_sys->i_metadata_size)
        goto bail;
    /* is it valid? */
    rc = AMF_Decode(&s_obj_onMetaData, p_sys->p_metadata_data + 1, p_sys->i_metadata_size - 1, true);
    if (rc < 0)
        goto bail;
    p_sys->i_output_file_valid = -1;
    p_prop_onMetaData_properties = AMF_GetProp(&s_obj_onMetaData, NULL, 0);
    if (p_prop_onMetaData_properties->p_type != AMF_ECMA_ARRAY)
        goto bail;
    for (n = 0; n < (unsigned) AMF_CountProp(&p_prop_onMetaData_properties->p_vu.p_object); n++)
    {
        p_property = AMF_GetProp(&p_prop_onMetaData_properties->p_vu.p_object, NULL, n);
        /* is it complete */
        if ((p_property->p_type == AMF_NUMBER) && (p_property->p_name.av_len == 8) && !memcmp("filesize", p_property->p_name.av_val, 8))
        {
            if (s_stat_buffer.st_size == (off_t) p_property->p_vu.p_number)
            {
                p_sys->i_output_file_complete = -1;
                p_sys->i_size = s_stat_buffer.st_size;
                break;
            }
        }
    }
bail:
    if (s_obj_onMetaData.o_props)
        AMF_Reset(&s_obj_onMetaData);
    if (p_sys->p_metadata_data)
    {
        free(p_sys->p_metadata_data);
        p_sys->p_metadata_data = NULL;
        p_sys->i_metadata_size = 0;
    }
    if (!p_sys->i_output_file_valid)
    {
        close(p_sys->i_output_file_rfd);
        p_sys->i_output_file_rfd = 0;
    }

    return p_sys->i_output_file_valid ? 0 : -1;
}

static int init_output_file(stream_t *s)
{
    stream_sys_t *p_sys = s->p_sys;

    if (!p_sys->i_output_file_valid)
        p_sys->i_output_file_wfd = open(p_sys->psz_output_file, O_CREAT | O_WRONLY | O_TRUNC, 0644);
    else if (!p_sys->i_output_file_complete)
        p_sys->i_output_file_wfd = open(p_sys->psz_output_file, O_WRONLY);
    else
        p_sys->i_output_file_wfd = 0;
    if (p_sys->i_output_file_wfd < 0)
        return -1;
    if (!p_sys->i_output_file_complete)
        p_sys->i_output_file_rfd = open(p_sys->psz_output_file, O_RDONLY);
    if (p_sys->i_output_file_rfd < 0)
    {
        if (p_sys->i_output_file_wfd > 0)
        {
            close(p_sys->i_output_file_wfd);
            p_sys->i_output_file_wfd = 0;
        }
        return -1;
    }
    return 0;
}

static int Open(vlc_object_t *p_this)
{
    stream_t *s = (stream_t *) p_this;
    stream_sys_t *p_sys;
    int rc;
    const uint8_t *p_peek_data;
    int64_t i_peek_size;

    /* example: http://v.iask.com/v_play.php?vid=71442057 */
    if (strcmp(s->psz_access, "http") || strncmp(s->psz_path, "v.iask.com/v_play.php?vid=", 26))
    //if (strcmp(s->psz_access, "http") || strncmp(s->psz_path, "127.0.0.1/sina/sina.xml", 23))
        return VLC_EGENERIC;
    /* */
    i_peek_size = stream_Peek(s->p_source, &p_peek_data, 4096);
    if (i_peek_size < 5 || memcmp(p_peek_data, "<?xml", 5))
        return VLC_EGENERIC;
    /* misc */
    p_sys = calloc(1, sizeof(stream_sys_t));
    if (!p_sys)
        return VLC_ENOMEM;
    s->p_sys = p_sys;
    /* */
    p_sys->i_offset = 0;
    p_sys->i_mode = var_InheritInteger(s, "flv-filter-mode");
    p_sys->i_seek_method = var_InheritInteger(s, "flv-filter-seek-method");
    if (p_sys->i_mode == 0)
    {
        if (init_buffer(s) < 0)
        {
            msg_Err(s, "Could not initialize buffer(%zd bytes).", p_sys->i_buffer_size);
            goto bail1;
        }
    }
    else if (p_sys->i_mode == 1)
    {
        /* test the given file */
        test_output_file(s);
        msg_Dbg(s, "flv-output-file valid = %d, complete = %d", p_sys->i_output_file_valid, p_sys->i_output_file_complete);
        /* open the given file */
        if (init_output_file(s) < 0)
        {
            msg_Err(s, "Could not initialize output file(`%s\').", p_sys->psz_output_file);
            goto bail1;
        }
        /* it is ready now */
        if (p_sys->i_output_file_complete)
            sina_v_play_set_status(s, STREAM_STATUS_STARTED);
    }
    else
    {
        msg_Err(s, "FLV stream filter mode %d is invalid.", p_sys->i_mode);
        goto bail1;
    }
    /* trust complete file */
    if ((p_sys->i_mode != 1) || !p_sys->i_output_file_complete)
    {
        if (sina_v_play_durl_parse_init(s, (const char *) p_peek_data, i_peek_size) < 0)
            goto bail1;
    }
    /* for http access module*/
    if (var_Create(s, "http-reconnect", VLC_VAR_BOOL) == VLC_SUCCESS)
        var_SetBool(s, "http-reconnect", true);
    else
        msg_Warn(s, "http-reconnect may be disabled.");
    if (var_Create(s, "http-continuous", VLC_VAR_BOOL) == VLC_SUCCESS)
        var_SetBool(s, "http-continuous", true);
    else
        msg_Warn(s, "http-continuous may be disabled.");
    /* spawn the download thread if necessary */
    if ((p_sys->i_mode != 1) || !p_sys->i_output_file_complete)
    {
        vlc_mutex_init(&p_sys->buffer_lock);
        vlc_cond_init(&p_sys->buffer_filled_cond);
        vlc_cond_init(&p_sys->buffer_consumed_cond);
        rc = vlc_clone(&p_sys->sina_v_play_thread, p_sys->i_mode ? flv_downloading_thread : flv_streaming_thread, s, VLC_THREAD_PRIORITY_INPUT);
        if (rc < 0)
        {
            msg_Err(s, "Could not spawn."); 
            goto bail2;
        }
    }
    /* setup callbacks */
    s->pf_read = Read;
    s->pf_peek = Peek;
    s->pf_control = Control;
    return VLC_SUCCESS;
bail2:
    vlc_mutex_destroy(&p_sys->buffer_lock);
    vlc_cond_destroy(&p_sys->buffer_filled_cond);
    vlc_cond_destroy(&p_sys->buffer_consumed_cond);
    sina_v_play_durl_parse_free(s);
bail1:
    free(s->p_sys);
    s->p_sys = NULL;
    return VLC_EGENERIC;
}

static void Close(vlc_object_t *p_this)
{
    stream_t *s = (stream_t *) p_this;
    stream_sys_t *p_sys = s->p_sys;

    if (p_sys->sina_v_play_thread)
    {
        vlc_mutex_lock(&p_sys->buffer_lock);
        p_sys->i_status |= STREAM_STATUS_ABORTED;
        vlc_cond_signal(&p_sys->buffer_consumed_cond);
        vlc_mutex_unlock(&p_sys->buffer_lock);
        vlc_join(p_sys->sina_v_play_thread, NULL);
        vlc_mutex_destroy(&p_sys->buffer_lock);
        vlc_cond_destroy(&p_sys->buffer_filled_cond);
        vlc_cond_destroy(&p_sys->buffer_consumed_cond);
    }
    sina_v_play_durl_parse_free(s);
    if (p_sys->p_buffer_data)
        free(p_sys->p_buffer_data);
    if (p_sys->p_metadata_data)
        free(p_sys->p_metadata_data);
    if (p_sys->p_tag_data)
        free(p_sys->p_tag_data);
    if (p_sys->p_peek_data)
        free(p_sys->p_peek_data);
    free(p_sys);
}

static int GenericRead0(stream_t *s, uint8_t *p_data, unsigned int i_size, int i_read)
{
    stream_sys_t *p_sys = s->p_sys;
    int needed, once, received, start, count;

    needed = i_size;
    received = 0;
    while (needed > 0)
    {
        vlc_mutex_lock(&p_sys->buffer_lock);
        while ((p_sys->i_buffer_count == 0) && (p_sys->i_offset < p_sys->i_size))
        {
            vlc_cond_wait(&p_sys->buffer_filled_cond, &p_sys->buffer_lock);
            if ((p_sys->i_status & STREAM_STATUS_ERROR) || (p_sys->i_status & STREAM_STATUS_ABORTED))
                break;
        }
        if (i_read)
        {
            start = p_sys->i_buffer_start;
            count = p_sys->i_buffer_count;
        }
        else
        {
            start = (p_sys->i_buffer_start + received) % p_sys->i_buffer_size;
            count = p_sys->i_buffer_count - received;
        }
        once = (count >= needed) ? needed : count;
        if (once > 0)
        {
            if (p_data != NULL)
            {
                if ((unsigned)(once + start) > p_sys->i_buffer_size)
                {
                    memcpy(p_data + received, p_sys->p_buffer_data + start, p_sys->i_buffer_size - start);
                    memcpy(p_data + received + p_sys->i_buffer_size - start, p_sys->p_buffer_data, once + start - p_sys->i_buffer_size);
                }
                else
                    memcpy(p_data + received, p_sys->p_buffer_data + start, once);
            }
            if (i_read)
            {
                p_sys->i_buffer_start += once;
                p_sys->i_buffer_start %= p_sys->i_buffer_size;
                p_sys->i_buffer_count -= once;
                p_sys->i_offset += once;
                vlc_cond_signal(&p_sys->buffer_consumed_cond);
            }
        }
        vlc_mutex_unlock(&p_sys->buffer_lock);
        needed -= once;
        received += once;
        if ((p_sys->i_offset >= p_sys->i_size) || (p_sys->i_status & STREAM_STATUS_ERROR) || (p_sys->i_status & STREAM_STATUS_ABORTED))
            break;
    }

    return received;
}

static int GenericRead1(stream_t *s, uint8_t *p_data, unsigned int i_size, int i_read)
{
    stream_sys_t *p_sys = s->p_sys;
    off_t i_offset = -1;
    unsigned int needed, once, received;

    /* restore offset */
    i_offset = lseek(p_sys->i_output_file_rfd, 0, SEEK_CUR);
    if ((i_offset == (off_t) -1) || (((unsigned) i_offset != p_sys->i_offset) && (lseek(p_sys->i_output_file_rfd, p_sys->i_offset, SEEK_SET) < 0)))
    {
        msg_Err(s, "Error occured: (%m).");
        return 0;
    }
    /* do actual read */
    needed = i_size > (p_sys->i_size - p_sys->i_offset) ? (p_sys->i_size - p_sys->i_offset) : i_size;
    received = 0;
    while (needed > 0)
    {
        if (!p_sys->i_output_file_complete)
        {
            vlc_mutex_lock(&p_sys->buffer_lock);
            while (p_sys->i_offset + needed >= p_sys->i_current_offset)
            {
                vlc_cond_wait(&p_sys->buffer_filled_cond, &p_sys->buffer_lock);
                if ((p_sys->i_status & STREAM_STATUS_ERROR) || (p_sys->i_status & STREAM_STATUS_ABORTED))
                    break;
            }
            vlc_mutex_unlock(&p_sys->buffer_lock);
            if ((p_sys->i_status & STREAM_STATUS_ERROR) || (p_sys->i_status & STREAM_STATUS_ABORTED))
                break;
        }
        once = read(p_sys->i_output_file_rfd, p_data + received, needed);
        if (once <= 0)
            break;
        received += once;
        needed -= once;
        if (i_read)
            p_sys->i_offset += once;
    }

    return received;
}

static int Seek0(stream_t *s, uint64_t i_position)
{
    stream_sys_t *p_sys = s->p_sys;
    int i_fail = 0;

    /* basic checks */
    if (!p_sys->i_seek_point_size || !p_sys->p_seek_point_data)
        return -1;
    if (i_position > p_sys->i_size)
        return -1;
    if (p_sys->i_offset == i_position)
        return 0;
    /* seeking is not finished */
    if (p_sys->i_seek_state)
        return -1;
    /* wait until seeking is done */
    vlc_mutex_lock(&p_sys->buffer_lock);
    p_sys->i_seek_state = -1;
    p_sys->i_seek_offset = i_position;
    vlc_cond_signal(&p_sys->buffer_consumed_cond);
    while (p_sys->i_seek_state)
    {
        vlc_cond_wait(&p_sys->buffer_filled_cond, &p_sys->buffer_lock);
        /* I/O failed or aborted */
        if ((p_sys->i_status & STREAM_STATUS_ERROR) || (p_sys->i_status & STREAM_STATUS_ABORTED))
        {
            i_fail = -1;
            break;
        }
    }
    /* is seek done? */
    if (!i_fail)
        i_fail = (p_sys->i_offset == p_sys->i_seek_offset) ? 0 : -1;
    vlc_mutex_unlock(&p_sys->buffer_lock);

    return (i_fail ? -1 : 0);
}

static int Seek1(stream_t *s, uint64_t i_position)
{
    stream_sys_t *p_sys = s->p_sys;
    int rc;

    if (i_position > p_sys->i_size)
        return -1;
    if (p_sys->i_offset == i_position)
        return 0;
    if (!p_sys->i_output_file_complete && i_position >= p_sys->i_current_offset)
        return -1;
    rc = lseek(p_sys->i_output_file_rfd, i_position, SEEK_SET);
    if (rc < 0)
    {
        msg_Err(s, "Error occured: (%m).");
        return -1;
    }
    p_sys->i_offset = i_position;
    return 0;
}

static int Peek(stream_t *s, const uint8_t **pp_peek, unsigned int i_peek)
{
    stream_sys_t *p_sys = s->p_sys;
    void *tmp;
    int received;

    /* is it all inside cache? */
    if ((p_sys->i_peek_offset <= p_sys->i_offset) && ((p_sys->i_peek_offset + p_sys->i_peek_size) >= (p_sys->i_offset + i_peek)))
    {
        *pp_peek = (uint8_t *) (p_sys->p_peek_data + (p_sys->i_offset - p_sys->i_peek_offset));
        return i_peek;
    }
    /* make room */
    if (i_peek > (unsigned) p_sys->i_peek_data_size)
    {
        tmp = realloc(p_sys->p_peek_data, i_peek);
        if (!tmp)
        {
            msg_Err(s, "Could not allocate %d bytes for peeking.", i_peek);
            return 0;
        }
        p_sys->p_peek_data = tmp;
        p_sys->i_peek_data_size = i_peek;
    }
    /* save current state */
    p_sys->i_peek_offset = p_sys->i_offset;
    p_sys->i_peek_size = i_peek;
    *pp_peek = (uint8_t *) p_sys->p_peek_data;
    /* do the actual peeking */
    if (p_sys->i_mode == 0)
        received = GenericRead0(s, (uint8_t *) p_sys->p_peek_data, i_peek, 0);
    else if (p_sys->i_mode == 1)
        received = GenericRead1(s, (uint8_t *) p_sys->p_peek_data, i_peek, 0);
    else
        received = 0;

    return received;
}

static int Read(stream_t *s, void *buffer, unsigned int i_read)
{
    stream_sys_t *p_sys = s->p_sys;
    int received;

    /* do the actual reading */
    if (p_sys->i_mode == 0)
        received = GenericRead0(s, (uint8_t *) buffer, i_read, -1);
    else if (p_sys->i_mode == 1)
        received = GenericRead1(s, (uint8_t *) buffer, i_read, -1);
    else
        received = 0;

    return received;
}

static int Seek(stream_t *s, uint64_t i_position)
{
    stream_sys_t *p_sys = s->p_sys;

    if (p_sys->i_mode == 0)
        return Seek0(s, i_position);
    else if (p_sys->i_mode == 1)
        return Seek1(s, i_position);

    return -1;
}

static int Control(stream_t *s, int i_query, va_list args)
{
    stream_sys_t *p_sys = s->p_sys;
    int status;

    /* */
    if (i_query == STREAM_CAN_FASTSEEK)
    {
        *(va_arg (args, bool *)) = false;
        return VLC_SUCCESS;
    }
    /* */
    if (!p_sys->i_status)
        status = sina_v_play_wait_until_status(s, STREAM_STATUS_STARTED, 0);
    else
        status = p_sys->i_status;
    if ((status & STREAM_STATUS_ERROR) || (status & STREAM_STATUS_ABORTED))
        return VLC_EGENERIC;    
    /* */
    switch (i_query)
    {
        case STREAM_GET_POSITION:
        {
            *(va_arg (args, uint64_t *)) = p_sys->i_offset;
            break;
        }
        case STREAM_GET_SIZE:
        {
            *(va_arg (args, uint64_t *)) = p_sys->i_size;
            break;
        }
        case STREAM_CAN_SEEK:
        {
            *(va_arg (args, bool *)) = (p_sys->i_mode == 0) ? true : (p_sys->p_seek_point_data ? true : false);
            break;
        }
        case STREAM_SET_POSITION:
        {
            uint64_t i_position = (uint64_t) va_arg(args, uint64_t);
            if (Seek(s, i_position) < 0)
                return VLC_EGENERIC;
            break;
        }
        default:
            return VLC_EGENERIC;
    }
    return VLC_SUCCESS;
}

